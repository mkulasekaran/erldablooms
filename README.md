erldablooms is the erlang port of dablooms

**Function reference:**
new(count,false positive count,path of the file) - return reference

remove(reference,record) 

add(reference,record)

exists(reference,record)- return exists or not

sync(reference) - sync to the underlying file

example:
```
mithran@mithran-ubuntu:~/erldablooms$ erl -pa ebin/
Erlang/OTP 17 [erts-6.2] [source] [64-bit] [smp:8:8] [async-threads:10] [hipe] [kernel-poll:false]

Eshell V6.2  (abort with ^G)
1> {ok,Ref}=dablooms:new(1000,0.001,"/home/mithran/syncfile").
{ok,<<>>}
2> dablooms:add(Ref,<<"record1">>).
ok
3> dablooms:exists(Ref,<<"record1">>).
true
4> dablooms:remove(Ref,<<"record1">>).
ok
5> dablooms:exists(Ref,<<"record1">>).
false
```