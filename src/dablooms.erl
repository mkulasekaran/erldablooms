
-module(dablooms).
-author('Mithran Kulasekaran').
-export([new/3,add/2,remove/2,exists/2,sync/1]).

-on_load(init/0).

init() ->
    case code:priv_dir(dablooms) of
        {error, bad_name} ->
            case code:which(?MODULE) of
                Filename when is_list(Filename) ->
                    SoName = filename:join([filename:dirname(Filename),"../priv", "dablooms_nifs"]);
                _ ->
                    SoName = filename:join("../priv", "dablooms_nifs")
            end;
        Dir ->
            SoName = filename:join(Dir, "dablooms_nifs")
    end,
    erlang:load_nif(SoName, 0).



new(_Arg1,_Arg2,_Arg3) ->
    case random:uniform(999999999999) of
        666 -> {ok, make_ref()};
        _   -> exit("NIF library not loaded")
    end.

remove(_Ref, _Bin) ->
    case random:uniform(999999999999) of
        666 -> ok;
        _   -> exit("NIF library not loaded")
    end.

add(_Ref, _Bin) ->
    case random:uniform(999999999999) of
        666 -> ok;
        _   -> exit("NIF library not loaded")
    end.

exists(_Ref, _Bin) ->
    case random:uniform(999999999999) of
        666 -> ok;
        _   -> exit("NIF library not loaded")
    end.

sync(_Ref) ->
    case random:uniform(999999999999) of
        666 -> true;
        667 -> false;
        _   -> exit("NIF library not loaded")
    end.

