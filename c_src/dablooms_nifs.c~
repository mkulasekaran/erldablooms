/*
 * dablooms_nifs.c
 *
 *  Created on: Aug 10, 2014
 *      Author: mithu
 */

#include "dablooms_nifs.h"
#include "erl_nif_compat.h"
#include "dablooms.h"
#include <time.h>
#include <string.h>
#define MAXBUFLEN  1024
#define STRLEN 32

static ErlNifResourceType* DABLOOMS_RESOURCE;

typedef struct {
	scaling_bloom_t *bloom;
} bhandle;

ERL_NIF_TERM dablooms_new_resource(ErlNifEnv* env, int argc,
		const ERL_NIF_TERM argv[]);
ERL_NIF_TERM dablooms_add(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM dablooms_remove(ErlNifEnv* env, int argc,
		const ERL_NIF_TERM argv[]);
ERL_NIF_TERM dablooms_exists(ErlNifEnv* env, int argc,
		const ERL_NIF_TERM argv[]);
ERL_NIF_TERM dablooms_sync(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]);

void dablooms_resource_dtor(ErlNifEnv* env, void* arg);

int on_load(ErlNifEnv* env, void** priv_data, ERL_NIF_TERM load_info);

static ErlNifFunc nif_funcs[] = { { "new", 3, dablooms_new_resource }, { "add",
		2, dablooms_add }, { "remove", 2, dablooms_remove }, { "exists", 2,
		dablooms_exists }, { "sync", 1, dablooms_sync }, };

ERL_NIF_INIT(dablooms, nif_funcs, &on_load, NULL, NULL, NULL);

ERL_NIF_TERM dablooms_new_resource(ErlNifEnv* env, int argc,
		const ERL_NIF_TERM argv[]) {
	bhandle* handle = (bhandle*) enif_alloc_resource_compat(env,
			DABLOOMS_RESOURCE, sizeof(bhandle));
	int temp;
	unsigned int c;
	double e;
	char* value;

	if (enif_get_int(env, argv[0], &temp) && enif_get_double(env, argv[1], &e)
			&& enif_get_string(env, argv[2], value, MAXBUFLEN,
					ERL_NIF_LATIN1)) {
		c = (temp >= 0 ? temp : -temp);
		handle->bloom = new_scaling_bloom(c, e, value);
		ERL_NIF_TERM result = enif_make_resource(env, handle);
		enif_release_resource_compat(env, handle);
		return enif_make_tuple2(env, enif_make_atom(env, "ok"), result);
	} else {
		return enif_make_badarg(env);
	}
}

ERL_NIF_TERM dablooms_add(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
	bhandle* handle;
	char* value;
	if (enif_get_resource(env, argv[0], DABLOOMS_RESOURCE, (void**) &handle)
			&& enif_get_string(env, argv[1], value, MAXBUFLEN,
					ERL_NIF_LATIN1)) {
		scaling_bloom_add(handle->bloom, value, strlen(value), time(NULL));

		return enif_make_atom(env, "ok");
	} else {
		return enif_make_badarg(env);
	}
}

ERL_NIF_TERM dablooms_remove(ErlNifEnv* env, int argc,
		const ERL_NIF_TERM argv[]) {
	bhandle* handle;
	char* value;
	if (enif_get_resource(env, argv[0], DABLOOMS_RESOURCE, (void**) &handle)
			&& enif_get_string(env, argv[1], value, MAXBUFLEN,
					ERL_NIF_LATIN1)) {
		scaling_bloom_remove(handle->bloom, value, strlen(value), time(NULL));

		return enif_make_atom(env, "ok");
	} else {
		return enif_make_badarg(env);
	}
}

ERL_NIF_TERM dablooms_exists(ErlNifEnv* env, int argc,
		const ERL_NIF_TERM argv[]) {
	bhandle* handle;
	char* value;
	if (enif_get_resource(env, argv[0], DABLOOMS_RESOURCE, (void**) &handle)
			&& enif_get_string(env, argv[1], value, MAXBUFLEN,
					ERL_NIF_LATIN1)) {
		int count = scaling_bloom_check(handle->bloom, value, strlen(value));
		return enif_make_int(env, count);
	} else {
		return enif_make_badarg(env);
	}
}

ERL_NIF_TERM dablooms_sync(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
	bhandle* handle;
	if (enif_get_resource(env, argv[0], DABLOOMS_RESOURCE, (void**) &handle)) {
		bitmap_flush(handle->bloom->bitmap);
		return enif_make_atom(env, "ok");
	} else {
		return enif_make_badarg(env);
	}
}

void dablooms_resource_dtor(ErlNifEnv* env, void* arg) {
	bhandle* handle = (bhandle*) arg;
	bitmap_flush(handle->bloom->bitmap);

}

int on_load(ErlNifEnv* env, void** priv_data, ERL_NIF_TERM load_info) {
	ErlNifResourceFlags flags = (ErlNifResourceFlags) (ERL_NIF_RT_CREATE
			| ERL_NIF_RT_TAKEOVER);
	DABLOOMS_RESOURCE = enif_open_resource_type_compat(env,
			"dablooms_resource", &dablooms_resource_dtor, flags, 0);
	return 0;
}


